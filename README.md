INTRODUCTION
------------
The Curator IO module provides a Drupal based block implementation of  
[Curator IO](https://curator.io/ "Curator IO")
(Brandable social media aggregator).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/curatorio

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/node/add/project-issue/curatorio
   
REQUIREMENTS
------------
No special requirements  

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   
CONFIGURATION
-------------   
1.  After installation of this module, go to  **Structure**  ->  
**Block Layout**  ->  **Place block**
2.  Provide your unique FEED_ID and select options which suits your need
3.  Your block will now appear at your desired region with the feed, 
which you have constructed at  [Curator IO](https://curator.io/ "Curator IO")

Curator feed supports [Facebook](http://facebook.com/),
 [Instagram](http://instagram.com/), [Twitter](http://twitter.com/), 
 [Pinterest](http://pinterest.com/), [YouTube](https://youtube.com/), 
 [Google+](https://plus.google.com/), [Flickr](http://flickr.com/), 
 [Tumblr](http://tumblr.com/), [Vine](http://vine.co/), 
 [LinkedIn](https://www.linkedin.com/) and [RSS](https://feedly.com/).
 
With this module in place, you can show curator feed in 4 different ways.

1. **Waterfall**
2. **Carousel**
3. **Grid**
4. **Panel**

MAINTAINERS
-----------

Current maintainers:
 * Arijit Sarkar - https://www.drupal.org/u/arijitsdrush
