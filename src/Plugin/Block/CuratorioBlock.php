<?php

namespace Drupal\curatorio\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'CuratorioBlock' block.
 *
 * @Block(
 *  id = "curatorio_block",
 *  admin_label = @Translation("Curator Io widget"),
 * )
 */
class CuratorioBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['curator_io_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Curator IO Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['curator_io_settings']['feed_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter your unique Feed Id'),
      '#default_value' => isset($this->configuration['feed_id']) ? $this->configuration['feed_id'] : NULL,
      '#maxlength' => 256,
      '#size' => 64,
      '#description' => $this->t('Set your unique feed id found at curator io'),
    ];

    $options = [
      'Carousel' => 'Carousel',
      'Grid' => 'Grid',
      'Panel' => 'Panel',
      'Waterfall' => 'Waterfall',
    ];

    $form['curator_io_settings']['widget_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Selected'),
      '#options' => $options,
      '#default_value' => isset($this->configuration['widget_type']) ? $this->configuration['widget_type'] : NULL,
      '#description' => $this->t('Set widget type for curator io layout'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Clear cache tags on block submit.
    Cache::invalidateTags([$values['curator_io_settings']['feed_id'], $values['curator_io_settings']['widget_type']]);
    $this->configuration['feed_id'] = $values['curator_io_settings']['feed_id'];
    $this->configuration['widget_type'] = $values['curator_io_settings']['widget_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Build block.
    $build = [
      '#theme' => 'curatorio_io',
      '#id' => uniqid(),
      '#feed_id' => $this->configuration['feed_id'],
      '#widget_type' => $this->configuration['widget_type'],
      '#attached' => [
        'library' => [
          'curatorio/curatorio_assets',
        ],
      ],

      // Set cache tags based on feed_id and widget_type.
      '#cache' => [
        'tags' => [$this->configuration['feed_id'], $this->configuration['widget_type']],
      ],
    ];

    return $build;
  }

}
