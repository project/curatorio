/**
 * @file
 *  Core JS file.
 *  global: jQuery, Drupal
 */
$(document).ready(() => {
  // Push all ids into array for curator widget.
  const id_array = [];

  $(".curator-feed").each(function() {
    id_array.push(this.id);
  });

  // Curator Elements process.
  $.each(id_array, (index, value) => {
    const feed_id = $(`#${value}`).attr("data-feed-id");
    const widget_type = $(`#${value}`).attr("data-widget-type");

    if (widget_type === "Panel") {
      new Curator.Widgets.Panel({
        feedId: feed_id, // FEED_ID to load
        container: `#${value}`, // HTML element to hold the feed
        debug: false, // turn debugging on or off
        filter: {
          showNetworks: true, // show Networks filter bar
          showSources: true // show Sources filter bar
        }
      });
    } else if (widget_type === "Grid") {
      new Curator.Widgets.Grid({
        feedId: feed_id, // FEED_ID to load
        container: `#${value}`, // HTML element to hold the feed
        debug: false, // turn debugging on or off
        filter: {
          showNetworks: true, // show Networks filter bar
          showSources: true // show Sources filter bar
        }
      });
    } else if (widget_type === "Carousel") {
      new Curator.Widgets.Carousel({
        feedId: feed_id, // FEED_ID to load
        container: `#${value}`, // HTML element to hold the feed
        debug: false, // turn debugging on or off
        filter: {
          showNetworks: true, // show Networks filter bar
          showSources: true // show Sources filter bar
        }
      });
    } else {
      new Curator.Widgets.Waterfall({
        feedId: feed_id, // FEED_ID to load
        container: `#${value}`, // HTML element to hold the feed
        postsPerPage: 9, // number of posts per page
        debug: false, // turn debugging on or off
        filter: {
          showNetworks: true, // show Networks filter bar
          showSources: true // show Sources filter bar
        }
      });
    }
  });
});
