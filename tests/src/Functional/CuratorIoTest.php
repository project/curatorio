<?php

namespace Drupal\Tests\curatorio\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group curatorio_io
 */
class CuratorIoTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['system_test', 'block', 'curatorio'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Create and login user.
    $admin_user = $this->drupalCreateUser([
      'administer blocks', 'administer site configuration',
      'access administration pages',
    ]);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test that the Curator Io block can be placed and works.
   */
  public function testCuratorBlock() {
    // Test availability of the curator block in the admin "Place blocks" list.
    \Drupal::service('theme_handler')->install(['bartik', 'seven', 'stark']);
    $theme_settings = $this->config('system.theme');
    foreach (['bartik', 'seven', 'stark'] as $theme) {
      $this->drupalGet('admin/structure/block/list/' . $theme);
      // Configure and save the block.
      $this->drupalPlaceBlock('curatorio_block', [
        'label' => 'Curator Io widget',
        'feed_id' => '6d283c7b-251d-4a03-82e1-2223dd3aa861',
        'widget_type' => 'Grid',
        'region' => 'content',
        'theme' => $theme,
      ]);
      // Set the default theme and ensure the block is placed.
      $theme_settings->set('default', $theme)->save();
      $this->drupalGet('<home>');
      $this->assertText('Curator Io widget', 'Curator block found');
    }
  }

}
